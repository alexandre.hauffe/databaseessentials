package com.example.databaseEssentials.controler;

import com.example.databaseEssentials.dao.BookRepository;
import com.example.databaseEssentials.model.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class BookController {

    @Autowired
    private BookRepository repository;

    @PostMapping("/add")
    public String create(@RequestBody Book book){
        repository.save(book);
        return "Book "+ book.getId() + " add";
    }

    @GetMapping("/findAll")
    public List<Book> findAll(){
        return repository.findAll();
    }

    @GetMapping("/find/{id}")
    public Optional<Book> getById(@PathVariable int id){
        return repository.findById(id);
    }

    @DeleteMapping("/delete/{id}")
    public String delete(@PathVariable int id){
        repository.deleteById(id);
        return "book " + id + " deleted";
    }
}
