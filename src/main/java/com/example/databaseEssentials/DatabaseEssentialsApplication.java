package com.example.databaseEssentials;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DatabaseEssentialsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DatabaseEssentialsApplication.class, args);
	}

}
