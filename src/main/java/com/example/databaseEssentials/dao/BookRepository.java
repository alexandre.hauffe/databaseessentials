package com.example.databaseEssentials.dao;

import com.example.databaseEssentials.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookRepository extends MongoRepository<Book, Integer> {

}
